Command line instructions


Git global setup

git config --global user.name "Daniel Guedes"
git config --global user.email "daniel.guedes@gmail.com"

Create a new repository

git clone https://gitlab.com/danielguedes/docs-publico.git
cd docs-publico
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/danielguedes/docs-publico.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/danielguedes/docs-publico.git
git push -u origin --all
git push -u origin --tags
